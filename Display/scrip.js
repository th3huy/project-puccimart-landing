/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gPageNum;
var gBASE_URL = "https://pucci-mart.onrender.com/api";
var gPetObj;
var itemsPerPage = 8;
var totalItems;
var totalPage;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    // Thực hiện xử lý hiển thị của trang 1
    makecall(1);
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function makecall(vPagenum) {
    // ham load pizza co phan trang
    callApiLoadToDataPet(vPagenum);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

function createpagination(vPagenum) {

    $("#page-click").html("");
    // Hiển thị các trang trong phạm vi từ trang bắt đầu đến trang kết thúc

    if (vPagenum == 1) {
        $("#page-click").append(`<span class="back disabled previous">(</span>`);
    } else {
        $("#page-click").append(`<span class="back" onclick='makecall(${vPagenum - 1})'>(</span>`);
    }

    if (vPagenum == totalPage) {
        $("#page-click").append(`<span class="next disabled previous">)</span>`);
    } else {
        $("#page-click").append(`<span class="next" onclick='makecall(${vPagenum + 1})'>)</span>`);
    }
}

function callApiLoadToDataPet(vPagenum) {

    const vQueryParams = new URLSearchParams();
    vQueryParams.append('_limit', itemsPerPage);
    vQueryParams.append('_page', vPagenum);


    gPageNum = vPagenum
    $.ajax({
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        type: "GET",
        async: true,
        dataType: "json",
        success: function (dataObj) {
            $("#all-pet").html("");
            gPetObj = dataObj.rows;
            console.log(gPetObj);
            loadDataFilter(dataObj);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }
    });
};


function loadDataFilter(paramData) {
    console.log(paramData);
    totalItems = paramData.count;
    totalPage = Math.ceil(totalItems / itemsPerPage);
    for (var i = 0; i < paramData.rows.length; i++) {
        const bElement = paramData.rows[i];
        $("#all-pet").append(`<div class="card col-3 col-md-3 col-sm-6">
        <div class="image-group">
            <img style="width: 293px; height: 293px;" src= ${bElement.imageUrl} alt="">
        </div>
        <p class="lucky-description">${bElement.name}</p>
        <p class="gender-description">${bElement.description}</p>
        <p class="price-description">$${bElement.promotionPrice} <span>$${bElement.price}</span></p>
    </div>`)
    }
    createpagination(gPageNum);
}

